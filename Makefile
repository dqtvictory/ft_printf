LIBFT_DIR	= libft
LIBFT_FNAME	= libft.a

MANDA_DIR	= manda
MANDA_NAMES	= checks.c ft_printf.c print_char.c print_num.c utils.c
MANDA_SRCS	= $(addprefix $(MANDA_DIR)/, $(MANDA_NAMES))

BONUS_DIR	= bonus
BONUS_NAMES	= 	checks_bonus.c ft_printf_bonus.c print_c_bonus.c \
				print_d_bonus.c print_misc_bonus.c print_n_bonus.c \
				print_p_bonus.c print_s_bonus.c print_u_bonus.c \
				print_x_bonus.c utils_bonus.c
BONUS_SRCS	= $(addprefix $(BONUS_DIR)/, $(BONUS_NAMES))

MANDA_OBJS	= ${MANDA_SRCS:.c=.o}
BONUS_OBJS	= ${BONUS_SRCS:.c=.o}

NAME		= libftprintf.a

CC			= gcc
CFLAGS		= -Wall -Wextra -Werror

AR			= ar -rcs

RM			= rm -f

RL			= ranlib

.c.o:
			${CC} ${CFLAGS} -c $< -o ${<:.c=.o}

${NAME}:	${MANDA_OBJS}
			make -C ${LIBFT_DIR}
			cp ${LIBFT_DIR}/${LIBFT_FNAME} ${NAME}
			${AR} ${NAME} ${MANDA_OBJS}
			${RL} ${NAME}

bonus:		${BONUS_OBJS}
			make -C ${LIBFT_DIR}
			cp ${LIBFT_DIR}/${LIBFT_FNAME} ${NAME}
			${AR} ${NAME} ${BONUS_OBJS}
			${RL} ${NAME}

all:		${NAME}

clean:
			make -C ${LIBFT_DIR} clean
			${RM} ${MANDA_OBJS} ${BONUS_OBJS}

fclean:		clean
			make -C ${LIBFT_DIR} fclean
			${RM} ${NAME}

re:			fclean all

.PHONY:		all bonus clean fclean re makelib
