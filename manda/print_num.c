/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_num.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/07 21:27:40 by qdam              #+#    #+#             */
/*   Updated: 2021/05/14 19:00:00 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	print_p(va_list vl, t_opts *op)
{
	size_t	n;
	size_t	digits;
	size_t	len_n;

	n = va_arg(vl, size_t);
	digits = ft_len_u(n, 16);
	len_n = digits;
	if (op->pr >= 0)
		len_n += (op->pr - (int)digits) * (op->pr > (int)digits);
	else if (op->zr && op->wdth > (int)len_n)
		len_n = op->wdth;
	if (op->wdth > (int)len_n + 2 && !op->mns)
		pc_repeat(' ', op->wdth - (int)len_n - 2 + (!n && !op->pr), &op->cnt);
	ps("0x", 2, &op->cnt);
	if (len_n > digits)
		pc_repeat('0', len_n - digits, &op->cnt);
	if (n || op->pr)
	{
		ft_putfd_u_base(FD, n, BASE16LC);
		op->cnt += digits;
	}
	if (op->wdth > (int)len_n + 2 && op->mns)
		pc_repeat(' ', op->wdth - (int)len_n - 2 + (!n && !op->pr), &op->cnt);
}

void	print_d(va_list vl, t_opts *op)
{
	ssize_t	n;
	size_t	digits;
	size_t	len_n;

	n = va_arg(vl, int);
	digits = ft_len_u(-n * (n < 0) + n * (n > 0), 10);
	len_n = digits + (n < 0);
	if (op->pr >= 0)
		len_n += (op->pr - (int)digits) * (op->pr > (int)digits);
	else if (op->zr && !op->mns && op->wdth > (int)len_n)
		len_n = op->wdth;
	if (op->wdth >= (int)len_n && !op->mns)
		pc_repeat(' ', op->wdth - (int)len_n + (!n && !op->pr), &op->cnt);
	if (n < 0)
		pc('-', &op->cnt);
	if (len_n > digits + (n < 0))
		pc_repeat('0', len_n - digits - (n < 0), &op->cnt);
	if (n || op->pr)
	{
		ft_putfd_u_base(FD, -n * (n < 0) + n * (n > 0), BASE10);
		op->cnt += digits;
	}
	if (op->wdth > (int)len_n && op->mns)
		pc_repeat(' ', op->wdth - (int)len_n + (!n && !op->pr), &op->cnt);
}

void	print_u(va_list vl, t_opts *op)
{
	size_t	n;
	size_t	digits;
	size_t	len_n;

	n = va_arg(vl, unsigned int);
	digits = ft_len_u(n, 10);
	len_n = digits;
	if (op->pr >= 0)
		len_n += (op->pr - (int)digits) * (op->pr > (int)digits);
	else if (op->zr && !op->mns && op->wdth > (int)len_n)
		len_n = op->wdth;
	if (op->wdth >= (int)len_n && !op->mns)
		pc_repeat(' ', op->wdth - (int)len_n + (!n && !op->pr), &op->cnt);
	if (len_n > digits)
		pc_repeat('0', len_n - digits, &op->cnt);
	if (n || op->pr)
	{
		ft_putfd_u_base(FD, n, BASE10);
		op->cnt += digits;
	}
	if (op->wdth > (int)len_n && op->mns)
		pc_repeat(' ', op->wdth - (int)len_n + (!n && !op->pr), &op->cnt);
}

void	print_x(va_list vl, t_opts *op)
{
	size_t	n;
	size_t	digits;
	size_t	len_n;

	n = va_arg(vl, unsigned int);
	digits = ft_len_u(n, 16);
	len_n = digits;
	if (op->pr >= 0)
		len_n += (op->pr - (int)digits) * (op->pr > (int)digits);
	else if (op->zr && !op->mns && op->wdth > (int)len_n)
		len_n = op->wdth;
	if (op->wdth >= (int)len_n && !op->mns)
		pc_repeat(' ', op->wdth - (int)len_n + (!n && !op->pr), &op->cnt);
	if (len_n > digits)
		pc_repeat('0', len_n - digits, &op->cnt);
	if (n || op->pr)
	{
		if (op->spec == 'x')
			ft_putfd_u_base(FD, n, BASE16LC);
		else
			ft_putfd_u_base(FD, n, BASE16UC);
		op->cnt += digits;
	}
	if (op->wdth > (int)len_n && op->mns)
		pc_repeat(' ', op->wdth - (int)len_n + (!n && !op->pr), &op->cnt);
}
